<?php

namespace Users\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Users\Form\UsersForm;
use Users\Model\Users;

class UsersController extends AbstractActionController {
    protected $usersTable;
    
    public function indexAction() {
        // grab the paginator from the AlbumTable
         $paginator = $this->getUsersTable()->fetchAll(true);
         // set the current page to what has been passed in query string, or to 1 if none set
         $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
         // set the number of items per page to 10
         $paginator->setItemCountPerPage(10);
        return new ViewModel(array(
            // 'users' => $this->getUsersTable()->fetchAll(),
            'paginator' => $paginator,
        ));
    }
    public function editAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('users', array(
                        'action' => 'add'
            ));
        }

        // Get the Users with the specified id.  An exception is thrown
        // if it cannot be found, in which case go to the index page.
        try {
            $users = $this->getUsersTable()->getUser($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute('users', array(
                        'action' => 'index'
            ));
        }

        $form = new UsersForm();
        $form->bind($users);
        $form->get('submit')->setAttribute('value', 'Update');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $this->getUsersTable()->saveUser($users);

                // Redirect to list of users
                return $this->redirect()->toRoute('users');
            }
        }

        return array(
            'id' => $id,
            'form' => $form,
        );
    }

    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('users');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $this->getUsersTable()->deleteUser($id);
            }

            // Redirect to list of users
            return $this->redirect()->toRoute('users');
        }

        return array(
            'id' => $id,
            'users' => $this->getUsersTable()->getUsers($id)
        );
    }
    // module/Users/src/Users/Controller/UsersController.php:
    public function getUsersTable() {
        if (!$this->usersTable) {
            $sm = $this->getServiceLocator();
            $this->usersTable = $sm->get('Users\Model\UsersTable');
        }
        return $this->usersTable;
    }
    
    public function addAction() {
        $form = new UsersForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $users = new Users();
            $form->get('submit')->setAttribute('value', 'Add New User');
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $users->exchangeArray($form->getData());
                $this->getUsersTable()->saveUsers($users);
                // Redirect to list of users
                return $this->redirect()->toRoute('users');
            }
        }
        return array(
            'id' => $id,
            'form' => $form,
        );
    }
}

